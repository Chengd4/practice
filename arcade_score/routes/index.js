var express = require('express');
var router = express.Router();

var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/highscore');

var User = require('../models/users');
var Game = require('../models/games');
var Score = require('../models/scores');

var session = require('client-sessions');

router.use(session({
  cookieName: 'session',
  secret: 'blargadkjasldjlvjzx',
  duration: 30 * 60 * 1000,
  activeDuration: 5 * 60 * 1000,
}));

router.use(function(req, res, next) {
  if (req.session && req.session.user) {
    User.findOne({ username: req.session.user.username }, function(err, user) {
      if (user) {
        req.user = user;
        delete req.user.password; // delete the password from the session
        req.session.user = user;  //refresh the session value
        res.locals.user = user;
      }
      // finishing processing the middleware and run the route
      next();
    });
  } else {
    next();
  }
});



router.post('/login', function(req, res) {
  User.findOne({ username: req.body.username }, function(err, user) {
    if (!user) {
      res.render('index', { title: 'Arcade Game', error: 'Invalid username or password.' });
    } else {
      if (req.body.password === user.password) {
        // sets a cookie with the user's info
        req.session.user = user;
        res.redirect('/arcade');
      } else {
        res.render('login', { title: 'Arcade Game', error: 'Invalid username or password.' });
      }
    }
  });
});



/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Arcade Game', error: ''});
});

router.post('/register', function(req,res){
  var newuser = new User({tagname: req.body.tagname, username: req.body.username, password: req.body.password});
  newuser.save(function(err,doc){
    if (err){
      throw err;
    }
    res.redirect('/');
  });
});

function requireLogin (req, res, next) {
  if (!req.user) {
    res.redirect('/');
  } else {
    next();
  }
};

function displayarcade(req, res){
  Game.find(function(err,doc){
    res.render('arcade', {game: doc.sort(function(a,b){
      if (a.name > b.name){
        return 1;
      }
      if (a.name < b.name){
        return -1;
      }
      return 0;
    })} );
  });
}

router.get('/arcade', requireLogin, function(req, res) {
  displayarcade(req, res);
});

router.get('/newgame', requireLogin, function(req,res){
  res.render('newgame');
});

router.post('/newgame', function(req,res){
  var newgame = new Game({name: req.body.name});
  newgame.save(function(err,doc){
    if (err){
      throw err;
    }
    displayarcade(req, res);
  })
});

router.get("/game/:id", requireLogin, function(req,res){
  Game.findById(req.params.id, function(err,game){
    if (err){
      throw err;
    }
    Score.find({post: req.params.id}, function(err,scores){
      if (err){
        throw err;
      }
      res.render('highscore', {game: game.name, scores: scores.sort(function(a,b){ return b.score - a.score})});
    })
  })
});

router.post("/game/:id", requireLogin, function(req,res){
  var newscore = new Score({
    post: req.params.id,
    tagname: req.user.tagname,
    score: req.body.score
  })

  newscore.save(function(err,doc){
    if (err){
      throw err;
    }
    res.redirect("/game/" + req.params.id);
  })
});

router.get('/logout', function(req, res) {
  req.session.reset();
  res.redirect('/');
});


module.exports = router;
