var mongoose = require("mongoose");

var game = mongoose.model("game", {name: String, date: {type: Date, default:Date.now, index: true}});

module.exports = game;
