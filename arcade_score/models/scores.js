var mongoose = require("mongoose");

var score = mongoose.model("score", {post: {type: mongoose.Schema.Types.ObjectId, ref: 'post'}, tagname: String, score: Number, date: {type: Date, default:Date.now, index: true}});

module.exports = score;
